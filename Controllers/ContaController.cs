using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Armazem.Models;
using Armazem.Services;
using Armazem.Data;

namespace Armazem.Controllers
{
    [Route("/account")]
    public class ContaController : ControllerBase
    {
        private readonly UserManager<IdentityUser> userManager;

        private readonly SignInManager<IdentityUser> signInManager;

        public ContaController(UserManager<IdentityUser> userManager,
                                SignInManager<IdentityUser> signInManager)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        [HttpPost]
        [Route("register")]
        public async Task<ActionResult<Register>> Register([FromBody] Register model){   
            if(ModelState.IsValid){
                var user  = new IdentityUser{
                    UserName = model.Email,
                    Email = model.Email
                };

                var result = await userManager.CreateAsync(user, model.Password);

                if(result.Succeeded){
                    await signInManager.SignInAsync(user, isPersistent: false);
                    return NoContent();
                }

                foreach(IdentityError error in result.Errors){
                    ModelState.AddModelError("", error.Description);
                }
            }
            return model;
        }

        [HttpPost]
        [Route("login")]
        public async Task<ActionResult<Register>> Login([FromBody] Register model){   
            if(ModelState.IsValid){
                var result = await signInManager.PasswordSignInAsync(model.Email, model.Password, false, false);

                if(result.Succeeded)
                    return Content("Bem vindo");
                
                else{
                    throw new ArgumentException("login inválido.");
                }
            }
            return model;
        }
    }
}