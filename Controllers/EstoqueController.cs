using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Armazem.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Armazem.Models;
using Armazem.Data;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Armazem.Entities;
using System;

namespace Armazem.Controllers
{
    [ApiController]
    [Route("/stock")]
    [Authorize(Roles="Operador")]
    public class EstoqueController : ControllerBase
    {
        private readonly IMapper mapper;
        
        private readonly EstoqueService st;

        public EstoqueController(IMapper mapper, StockService st){
            this.mapper = mapper;
            this.st = st;
        }
        
        [HttpPut]
        [Route("updatestoque")]
        public async Task<ActionResult<Entities.Product>> PutStockProduct([FromBody] FormInpOut fIO){
            return await st.FormIO(fIO);
        }
    }
}