using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Armazem.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Armazem.Models;
using Armazem.Data;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Armazem.Entities;
using System;

namespace Armazem.Controllers
{
    [ApiController]
    [Route("/product")]
    public class ProdutoController : ControllerBase
    {
        private readonly IMapper mapper;
        
        private readonly ProductService ps;

        public ProdutoController(IMapper mapper, ProductService ps){
            this.mapper = mapper;
            this.ps = ps;
        }

        [HttpGet]
        [Authorize]
        public async Task<ActionResult<List<Models.ProductCli>>> GetProductsCli(){
            var products = await ps.ListProducts();

            var prodMap = mapper.Map<List<Models.ProductCli>>(products);

            return Ok(prodMap);
        }

        [HttpGet]
        [Route("operator")]
        [Authorize(Roles="Operador")]
        public async Task<ActionResult<List<Models.Product>>> GetProductsOp(){
            var products = await ps.ListProducts();

            var prodMap = mapper.Map<List<Models.Product>>(products);

            return Ok(prodMap);
        }

        [HttpPost]
        [Authorize(Roles="Cadastro")]
        public async Task<ActionResult<Entities.Product>> PostProducts([FromBody] Entities.Product product){
            if(ModelState.IsValid){
                return await ps.CreateProduct(product);
            }

            else{
                return BadRequest(ModelState);
            }
        }

        [HttpPut]
        [Route("updateproduct/{id:int}")]
        [Authorize(Roles="Operador")]
        public async Task<ActionResult<Entities.Product>> PutProduct([FromBody] Entities.Product product, int id){
            return await ps.UpdateProduct(product, id);
        }
       
    }
}