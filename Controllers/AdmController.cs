using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Armazem.Models;
using Armazem.Services;
using Armazem.Data;

namespace Armazem.Controllers
{
    [Route("/admin")]
    [Authorize(Roles="Admin")]
    public class AdmController : ControllerBase
    {
        private readonly RoleManager<IdentityRole> roleManager;

        private readonly UserManager<IdentityUser> userManager;


        public AdmController(RoleManager<IdentityRole> roleManager, UserManager<IdentityUser> userManager)
        {
            this.roleManager = roleManager;
            this.userManager = userManager;
        }

        [HttpPost]
        [Route("role")]
        public async Task<ActionResult<CreateRole>> CreateRole([FromBody] CreateRole model)
        {
            if (ModelState.IsValid)
            {
                IdentityRole identityRole = new IdentityRole
                {
                    Name = model.RoleName
                };

                IdentityResult result = await roleManager.CreateAsync(identityRole);

                if (result.Succeeded)
                {
                    return NoContent();
                }

                foreach (IdentityError error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }

            return model;
        }

        [HttpPost]
        [Route("userrole")]
        public async Task<ActionResult<UserRole>> UserRole([FromBody] UserRole model)
        {
            var role = await roleManager.FindByNameAsync(model.RoleName);

            if (role == null)
                throw new ArgumentException("Role inválido.");


            var user = await userManager.FindByNameAsync(model.UserName);

            if (!await userManager.IsInRoleAsync(user, role.Name))
            {
                IdentityResult result = await userManager.AddToRoleAsync(user, role.Name);
            }
            return model;
        }
    }
}