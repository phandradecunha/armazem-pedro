namespace Armazem.Models
{
    public class CadastroProd
    {

        public string idProd { get; set; }
        public string nomeProd { get; set; }
        public string descProd { get; set; }
        public string tipoProd { get; set; }
        public int qtd {get;set;}
    }
}