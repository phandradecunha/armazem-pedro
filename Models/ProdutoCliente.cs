namespace Armazem.Models
{
    public class ProdutoCLiente
    {
        public string nomeProd { get; set; }
        public string descProd { get; set; }
        public string tipoProd { get; set; }
    }
}