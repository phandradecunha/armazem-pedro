using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Armazem.Entities;
using Armazem.Data;
using Microsoft.IdentityModel.Tokens;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Armazem.Models;

namespace Armazem.Services
{
    public class Estoque
    {
        private readonly ArmazemContext context;

        public Estoque(ArmazemContext context)
        {
            this.context = context;
        }

         public async Task<ActionResult<Entities.Product>> FormIO(FormInpOut fIO){
            var product = context.Products.FirstOrDefault(p => p.idṔrod == fIO.idProd);

            if(fIO.TypeOp.ToLower().Equals("saída") || fIO.TypeOp.ToLower().Equals("saída")){
                if(product.InitialInvent > fIO.qtd){
                    product.InitialInvent -= fIO.qtd;
                    context.Update(product);
                }
                else
                    throw new ArgumentException("A quatidade desejada não está disponível!");                
            }

            else if(fIO.TypeOp.ToLower().Equals("entrada")){
                product.InitialInvent += fIO.qtd;
                context.Update(product);
            }

            await context.SaveChangesAsync();
            
            return product;
        }
    }
}