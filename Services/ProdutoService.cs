using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Armazem.Entities;
using Armazem.Data;
using Microsoft.IdentityModel.Tokens;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Armazem.Models;

namespace Armazem.Services
{
    public class ProductServic
    {
        private readonly ArmazemContext context;

        public ProdutoService(ArmazemContext context)
        {
            this.context = context;
        }

        public async Task<List<Entities.Product>> Products(){
            return await context.Produto.ToListAsync();
        }

        public async Task<Entities.produto> CreateProduto(Entities.Produto produto){
            if(produto.inicProd.Equals(null)){
                produto.inicProd = 0;
            }

            if(produto.TypeStrg.ToLower().Equals("milhar")){
                produto.inicProd *= 1000;
            }

            context.Add(produto);
                        
            await context.SaveChangesAsync();
            
            return produto;
        }

        
        public async Task<Entities.produto> UpdateProduct(Entities.produto produto, int idProd){
            var update = context.produto.FirstOrDefault(p => p.idProd == idProd);
            
            if(update != null){
                update.idProd = Produto.idProd;
                update.nomeProd = Produto.nomeProd;
                update.decProd = Produto.decProd;
                update.inicProd = Produto.inicProd;
                update.tipoProd = Produto.tipoProd;

                await context.SaveChangesAsync();
            }
            return update;
        }

    }
}
