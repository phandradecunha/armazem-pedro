namespace Armazem.Entities
{
    public class Product
    {   
        [Key]
        public int idProd { get; set; }

        public string nomeProd { get; set;}

        public string descProd { get; set; }
        public int inicProd { get; set; }

        public string tipoProd { get; set; }

    }
}