using AutoMapper;
using Armazem.Entities;
using Armazem.Models;

namespace Armazem.Profiles
{
    public class prodPerfil : Profile
    {
        public prodPerfil(){
            CreateMap<Entities.Product, Models.Product>();
        }
    }
}