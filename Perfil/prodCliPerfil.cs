using AutoMapper;
using Armazem.Entities;
using Armazem.Models;

namespace Armazem.Profiles
{
    public class prodCLiPerfil : Profile
    {
        public ProdCliPerfil(){
            CreateMap<Entities.Product, Models.ProductCli>();
        }
    }
}