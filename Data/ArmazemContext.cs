using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Armazem.Entities;

namespace Armazem.Data
{
    public class ArmazemContext : IdentityDbContext
    {
        public ArmazemContext(DbContextOptions<ArmazemContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder){
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Product>()
                .HasIndex(p => p.Code)
                .IsUnique();
        }

        public DbSet<Product> Products { get; set; }

    }
}
